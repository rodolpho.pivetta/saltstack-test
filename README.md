# Teste para vaga Orquestração/Devops

### Descrição


Este repositório contém um Vagrantfile que cria uma VM OpenSuse com saltstack instalado, 
expondo a 8080 em localhost encaminhada para a porta 80 dentro da vm. Sua missão é criar states
salt que instalem, configurem e rodem um servidor web dentro da vm.

Fique a vontade para escolher o webserver que quiser.

O arquivo que será servido deve ter o seguinte conteúdo:

```html
<html>
    <head>
        <title>
            A Simple HTML Document
        </title>
    </head>
    <body>
        <p>This is a very simple HTML document</p>
        <p>It only has two paragraphs</p>
    </body>
</html>
```# saltstack-test

Para isso é necessário criar os states no repositório, note que o Vagrantfile copia 
todo seu conteúdo para /srv/salt. Para aplicá-los basta logar na máquina via ssh e aplicar
o highstate.

Os states devem ser enviados via merge request (fork) ou zipados por email.